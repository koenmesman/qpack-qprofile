# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 15:37:49 2022

@author: Koen
"""

from qblox_instruments import PlugAndPlay, Cluster, Pulsar
from quantify_scheduler.instrument_coordinator import InstrumentCoordinator
from quantify_scheduler.instrument_coordinator.components.qblox import PulsarQRMComponent,PulsarQCMComponent
from qcodes import Instrument

Instrument.close_all()

def get_module_port(name):
    return name[14:]


def debug_cluster_ic_from_pnp(manual_modules=None, multithreading=False):
    """
    create IC from PnP, connecting to modules directly - not to the cluster itself.
    multithreading is optional
    
    By selecting the connected modules with <manual_modules>, as a list of slots,
    you avoid timeouts and connect faster.
    """
    IC = InstrumentCoordinator('IC', multithreading=multithreading)
    pulsars = []
    components = []
    with PlugAndPlay() as pnp:
        devices = pnp.list_devices()
        for serial in devices:
            ip = pnp.get_ip(serial)

            cluster = Cluster('cluster', ip)
            modules = cluster.modules
            for mod in modules:
                mod_type=None
                port = get_module_port(mod.name)
                if int(port) not in manual_modules:
                    continue
                int_port = 25000+int(port)
                try:
                    pulsars.append(Pulsar(port, ip, port=int_port, debug=1))
                    pulsars[-1].reference_source = "internal"
                    mod_type = pulsars[-1].instrument_type
                    is_rf = pulsars[-1].is_rf_type
    
                except:
                    pass
                if mod_type == 'QRM': #and not is_rf
                    components.append(PulsarQRMComponent(pulsars[-1]))
                    components[-1].reference_source = 'internal'
                    IC.add_component(components[-1])
    
                if mod_type == 'QCM': #and not is_rf
                    components.append(PulsarQCMComponent(pulsars[-1]))
                    components[-1].reference_source = 'internal'
                    IC.add_component(components[-1])
                    
            # close cluster
            Instrument.find_instrument('cluster').close()
    return IC, pulsars

