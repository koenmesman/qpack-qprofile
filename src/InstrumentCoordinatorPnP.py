# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 15:37:49 2022

@author: Koen
"""


from qblox_instruments import PlugAndPlay, Cluster, Pulsar
from quantify_scheduler.instrument_coordinator import InstrumentCoordinator
from quantify_scheduler.instrument_coordinator.components.qblox import PulsarQRMComponent,PulsarQCMComponent, ClusterComponent
from qcodes import Instrument


def InstrumentCoordinator_Plug_and_Play():
    ic = InstrumentCoordinator('IC')
    clusters = []
    pulsars = []
    with PlugAndPlay() as pnp:
        devices = pnp.list_devices()
        for serial in devices:
            ip = pnp.get_ip(serial)
            name = pnp.get_name(serial)

            #clusters
            if devices[serial]['description']['model'] == 'cluster_mm':
                clusters.append(Cluster('cluster', ip))
                ic.add_component(ClusterComponent(clusters[-1]))

            #pulsars
            #print(devices[serial]['description']['model'][0:6])
            if devices[serial]['description']['model'][0:6] == 'pulsar':
               pulsars.append(Pulsar(name, ip))
               if pulsars[-1].instrument_type == 'QCM':
                   ic.add_component(PulsarQCMComponent(pulsars[-1]))
               if pulsars[-1].instrument_type == 'QRM':
                   ic.add_component(PulsarQRMComponent(pulsars[-1])) 

    return ic, (clusters, pulsars)

#Instrument.close_all()
#IC, hw = InstrumentCoordinator_Plug_and_Play()
#print(Instrument._all_instruments)