#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 15:13:38 2022

@author: koen
"""

from qblox_instruments import Cluster, Pulsar, PulsarType
from quantify_scheduler import Schedule
from quantify_scheduler.gettables import ScheduleGettable
from quantify_scheduler.compilation import qcompile
from quantify_scheduler.device_under_test.quantum_device import QuantumDevice
from quantify_scheduler.device_under_test.transmon_element import BasicTransmonElement
from quantify_scheduler.instrument_coordinator import InstrumentCoordinator
from quantify_scheduler.instrument_coordinator.components.qblox import ClusterComponent, PulsarQRMComponent, PulsarQCMComponent
from quantify_scheduler.schedules.timedomain_schedules import rabi_sched
from quantify_core.data.handling import set_datadir, snapshot, gen_tuid, get_datadir
from qcodes import Instrument
from MockLo import MockLocalOscillator
import backend_classes as qblox_hardware

from hardware_config import HardwareConfig
from QPack import Benchmark

from IC_from_pnp import debug_cluster_ic_from_pnp

from quantify_scheduler.gettables_profiled import ProfiledScheduleGettable
set_datadir("quantify-data")

Instrument.close_all()

# setup mock lo and connect to all modules seperately
lo0 = MockLocalOscillator("lo0")
IC, modules = debug_cluster_ic_from_pnp(manual_modules = [2,4,6,8,10], multithreading=True)

conf_obj = HardwareConfig(module_config={

                                        '2': {'qubits':[0,1]}, #QCM
                                        '4': {'qubits':[0,1]}, #QRM
                                        '6': {'qubits':[2,3]}, #QCM-RF
                                        '8': {'qubits':[2,3]}  #QRM-RF
                                        }
                          )

# auto generate hw_config from script and add lo
hw_config = conf_obj.JSON
hw_config['lo0'] = {
        "instrument_type": "LocalOscillator",
        "frequency": 4.9e9,
        "power": 17
    }

# add quantum device
quantumdevice = QuantumDevice('TestDevice')
quantumdevice.instr_instrument_coordinator('IC')
quantumdevice.hardware_config(hw_config)

num_qubits=4
qubit = []
for i in range(num_qubits):
    qubit.append(BasicTransmonElement('q{}'.format(i)))
    qubit[-1].clock_freqs.f01(4.9e9)
    qubit[-1].clock_freqs.readout(4.8e9)
    qubit[-1].rxy.amp180(0.03)
    qubit[-1].measure.acq_delay(100e-9)
    quantumdevice.add_element(qubit[-1])

# set schedule
sched = rabi_sched

# create gettable
gettable = ProfiledScheduleGettable(quantumdevice, sched, {'pulse_amp': 0.05, 'pulse_duration': 1e-7, 'frequency': 5e6, 'qubit': 'q1'})

# run and get results
gettable.get()

#Let the gettable know that runs are completed and data is aggregated
gettable.close()

#plotting and logging functions
gettable.plot_profile("demo_plot")
log = gettable.log_profile()
