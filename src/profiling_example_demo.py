#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Demo on using the ProfiledSchedulableGettable (in its current form)

General:
-init hardware (cluster)
-hw config
-quantum device
-create/import schedule

New:
-ProfiledScheduleGettable

@author: koen
"""

import qblox_instruments
from qblox_instruments import Cluster
from qblox_instruments import Pulsar
import quantify_scheduler
from quantify_scheduler import Schedule
from quantify_scheduler.gettables import ScheduleGettable
from quantify_scheduler.compilation import qcompile
from quantify_scheduler.device_under_test.quantum_device import QuantumDevice
from quantify_scheduler.device_under_test.transmon_element import BasicTransmonElement
from quantify_scheduler.instrument_coordinator import InstrumentCoordinator
from quantify_scheduler.instrument_coordinator.components.qblox import ClusterComponent, PulsarQRMComponent
from quantify_scheduler.schedules.timedomain_schedules import rabi_sched
from quantify_core.data.handling import set_datadir, snapshot, gen_tuid, get_datadir
from qcodes import Instrument
from MockLo import MockLocalOscillator

import pandas
import json
from hardware_config import HardwareConfig
from quantify_scheduler.gettables_profiled import ProfiledScheduleGettable
set_datadir("quantify-data")



# add instruments
Instrument.close_all()

lo0 = MockLocalOscillator("lo0")

IC = InstrumentCoordinator('IC')
cluster = Cluster("cluster0", "192.168.0.2")
IC.add_component(ClusterComponent(cluster))

"""
#pnp...
from InstrumentCoordinatorPnP import InstrumentCoordinator_Plug_and_Play
IC, hw = InstrumentCoordinator_Plug_and_Play()
"""
#print(Instrument._all_instruments)

# Hardware config
conf_obj = HardwareConfig()
hw_config = conf_obj.JSON

# add LO
hw_config['lo0'] = {
        "instrument_type": "LocalOscillator",
        "frequency": 5e6,
        "power": 17
    }

#print(json.dumps(hw_config, indent=4))


# add quantum device
quantumdevice = QuantumDevice('TestDevice')
quantumdevice.instr_instrument_coordinator('IC')
quantumdevice.hardware_config(hw_config)

q1 = BasicTransmonElement('q1')
q1.clock_freqs.f01(5e6)
q1.clock_freqs.readout(5e6)
q1.rxy.amp180(0.03)
q1.measure.acq_delay(100e-9)
q1.measure.pulse_amp(0.05)
quantumdevice.add_element(q1)
sched = rabi_sched

gettable = ProfiledScheduleGettable(quantumdevice, sched, {'pulse_amp': 0.05, 'pulse_duration': 1e-7, 'frequency': 5e6, 'qubit': 'q1'})

# run and get results
gettable.get()

#Let the gettable know that runs are completed and data is aggregated
gettable.close()

#plotting and logging functions
gettable.plot_profile("demo_plot")
log = gettable.log_profile()
print(log)
